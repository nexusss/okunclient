
import QtQuick 2.7
import QtQuick.Window 2.2

import QtQuick.Controls 2.0
import QtQuick.Layouts 1.1

import "radar/PlotSettings"
import "radar"


ApplicationWindow {
    visible: true
    width: 920
    height: 550
    minimumHeight: 550
    readonly property string version : "Okun Client 0.6"
    title: qsTr(version)

//    header: ToolBar{
//        RowLayout{
//            anchors.fill: parent

//            ToolButton{
//                text: qsTr("Plot settings")
//                onClicked: pl.show();

//                Shortcut{
//                   sequence: "Ctrl+P"
//                   onActivated: rs.show()
//                }
//            }
//            Item {
//                Layout.fillWidth: true
//                Layout.fillHeight: true
//            }

//        }
//    }

    PlotSettings{
        id: pl

        onBackColorChanged: rPlot.backColor = backColor
//        onSweep1ColorChanged: rPlot.sweep1Color = sweep1Color
//        onSweep2ColorChanged: rPlot.sweep2Color = sweep2Color
        onBorderColorChanged: rPlot.borderColor = borderColor
        onSweepWidthChanged: rPlot.sweepWidth = sweepWidth

    }



    RowLayout{
        anchors.fill: parent

                RadarPlot{
                    id: rPlot
                    anchors.fill: parent
                    Layout.fillHeight: true
                    Layout.fillWidth: true
                    onStartBind: {
                        console.log("start bind")
                        inter.setPort(port)
                        inter.startTcpClient()
                    }

                    Component.onCompleted: {
                        pl.backColor = backColor

                        pl.borderColor = borderColor
                        pl.sweepWidth = sweepWidth
                    }
                }


    Interface{
        id: inter
        Layout.fillHeight: true
        okunSettings: oSettings
        onSoftwareSet: {            
            rPlot.setSoftwareNumber(softnumber)
        }
        onServerVersionChanged: {

            title = version + " " + str
        }
    }
    }

    OkunSettings{
        id: oSettings
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.verticalCenter: parent.verticalCenter
        visible: false
        onSendTresholdAndDelta: {
            rPlot.setTreshold(treshold)
            rPlot.setDelta(delta)
        }
    }

}
