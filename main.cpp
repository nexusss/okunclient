#include <QApplication>
#include <QQmlApplicationEngine>
#include "perchcontrol.h"
#include "radar/plotsettings.h"
#include "radar/perchdata.h"
#include "radar/chartviewsettings.h"
#include <iostream>


int main(int argc, char *argv[])
{
    qmlRegisterType<PlotSettings>("dokltd.plotSettings", 1, 0, "PlotSettings");
    qmlRegisterType<ChartViewSettings>("dokltd.chartViewSettings", 1, 0, "ChartViewSettings");
    qmlRegisterType<PerchControl>("olvia.perchControl", 1, 0, "PerchControl");
    qmlRegisterType<PerchData>("olvia.perchData", 1, 0, "PerchData");

    QCoreApplication::setAttribute(Qt::AA_EnableHighDpiScaling);
    QApplication app(argc, argv);

    QQmlApplicationEngine engine;
    engine.load(QUrl(QLatin1String("qrc:/main.qml")));    

    return app.exec();
}
