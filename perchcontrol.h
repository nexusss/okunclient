#ifndef PERCHCONTROL_H
#define PERCHCONTROL_H

#include <QObject>
#include <QTcpSocket>
#include <QTcpSocket>
#include <QHostAddress>
#include <QThread>
#include <QTimer>
#include <QRegExp>

#include <QMetaType>

class PerchControl : public QObject
{
    Q_OBJECT

    enum class MESSAGETYPE{SOFTNUMBER = 1,UDP = 2,SETTINGSSET};
    enum class ANSWERTYPE{CONNECT = 10,SOFTWARENUMBER};

    QThread tcpThread;
    QTcpSocket *tcpSocket;
    QTimer *reconectTimer;
    void readAnswerFromServer();
    void connectToServer();

    QHostAddress mip = QHostAddress("192.168.1.16");
    quint16 mport = 56421;
    quint16 mudpPort = 0;
    QString mserverVersion;


    Q_PROPERTY(QString ip READ ip WRITE setIp )
    Q_PROPERTY(quint16 port READ port WRITE setPort )
    Q_PROPERTY(quint16 udpport READ udpport WRITE setUDPPort )

    Q_PROPERTY(QString serverVersion READ serverVersion NOTIFY serverVersionChanged)



    QRegExp rx = QRegExp( "[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}" );


public:
    explicit PerchControl(QObject *parent = 0);
    QString ip();
    quint16 port();
    QString serverVersion();
    quint16 udpport();

    ~PerchControl();


signals:
    void connectError(QString error);
    void connected();
    void disconnected();
    void writeServer(QByteArray);

    void serverVersionChanged(QString);
    void softNumberIsSet(int softnumber);
    void sendSettings(quint8 leftI,quint8 leftQ, quint8 upI,quint8 upQ,quint8 rightI, quint8 rightQ,quint8 downI,quint8 downQ,float treshold, bool up, bool down, bool hamming);
public slots:
    void startServer();
    void setIp(QString ip);
    void setPort(quint16 port);
    void setUDPPort(quint16 port);
    void setSoftwareNumber(int softnumber);
    void setSendSettings(quint8 leftI,quint8 leftQ, quint8 upI,quint8 upQ,quint8 rightI, quint8 rightQ,quint8 downI,quint8 downQ,float treshold, bool up, bool down,bool hamming);

};

#endif // PERCHCONTROL_H
