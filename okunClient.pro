QT += qml quick network charts

CONFIG += c++11

SOURCES += main.cpp \
    perchcontrol.cpp \
    radar/plotsettings.cpp \
    radar/perchdata.cpp \
    radar/okunudpsocket.cpp \
    radar/chartviewsettings.cpp

RESOURCES += qml.qrc

# Additional import path used to resolve QML modules in Qt Creator's code model
QML_IMPORT_PATH =

# Default rules for deployment.
include(deployment.pri)

HEADERS += \
    perchcontrol.h \
    okunudpsocket.h \
    radar/plotsettings.h \
    radar/types.h \
    radar/perchdata.h \
    radar/okunudpsocket.h \
    radar/chartviewsettings.h
