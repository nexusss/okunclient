import QtQuick 2.0
import QtQuick.Controls 2.0

Button {
    property string imgSource: ""
    id: button

    background: Rectangle {
              implicitWidth: 10
              implicitHeight: 10
              opacity: enabled ? 1 : 0.3
              color: button.down ? "#353535" : "#828282"
              radius: 5
          }
    Image {

        fillMode: Image.PreserveAspectFit
        anchors.fill: parent
        source: imgSource
        visible: (imgSource === "") ? false: true
    }
    autoRepeat: true
}
