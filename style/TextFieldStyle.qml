import QtQuick 2.7
import QtQuick.Controls 2.0

TextField {
    id: textField
    topPadding: 0
    bottomPadding: 0
    rightPadding: 5
    width: contentWidth + leftPadding + rightPadding < 60 ? 60 : contentWidth + leftPadding + rightPadding

    background: Rectangle {
              implicitWidth: 60
              implicitHeight: 20
              color: {
                  textField.activeFocus ?
                              (textField.acceptableInput) ? "white" : "red"
                              :"transparent"
              }
              border.width: 2
              radius: 5
              border.color: textField.activeFocus ? "#4137f1" : "transparent"

          }

    onEditingFinished: {
        focus = false
    }
    onActiveFocusChanged: {
        if(activeFocus){
            textField.font.pointSize += 5
        }
        else{
            textField.font.pointSize -= 5
        }
    }
    Keys.onPressed: {
        if(activeFocus){
            if (event.key === Qt.Key_Escape) {
                focus = false
            }
        }
    }
}
