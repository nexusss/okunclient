#include "chartviewsettings.h"
#include <QDir>

ChartViewSettings::ChartViewSettings(QObject *parent) : QObject(parent),cnf(nullptr)
{
    saveTimer.setSingleShot(true);
    saveTimer.setInterval(1 * 1000);

    connect(&saveTimer,&QTimer::timeout,this,&ChartViewSettings::saveSettings);
}
void ChartViewSettings::initialize(QString settingsFileName){

    qreal d_rMinX1 = 0;
    qreal d_rMaxX1 = 1000;
    qreal d_rMinY1 = -500;
    qreal d_rMaxY1 = 500;

    qreal d_fMinX1 = 0;
    qreal d_fMaxX1 = 1000;
    qreal d_fMinY1 = 0;
    qreal d_fMaxY1 = 1000;

    qreal d_f2MinX1 = 0;
    qreal d_f2MaxX1 = 1000;
    qreal d_f2MinY1 = 0;
    qreal d_f2MaxY1 = 1000;

    QString settingsPath = QDir::homePath() + "/okunClient";


    QDir settingsDir;
    settingsDir.mkdir(settingsPath);

    if(cnf != nullptr)
        delete cnf;

    cnf = new QSettings(settingsPath + "/" + settingsFileName + ".ini", QSettings::IniFormat);

    if(QFileInfo::exists(settingsPath + "/" + settingsFileName + ".ini")){
        setrMinX1(cnf->value("rMinX1",d_rMinX1).toFloat());
        setrMaxX1(cnf->value("rMaxX1",d_rMaxX1).toFloat());
        setrMinY1(cnf->value("rMinY1",d_rMinY1).toFloat());
        setrMaxY1(cnf->value("rMaxY1",d_rMaxY1).toFloat());

        setfMinX1(cnf->value("fMinX1",d_fMinX1).toFloat());
        setfMaxX1(cnf->value("fMaxX1",d_fMaxX1).toFloat());
        setfMinY1(cnf->value("fMinY1",d_fMinY1).toFloat());
        setfMaxY1(cnf->value("fMaxY1",d_fMaxY1).toFloat());

        setf2MinX1(cnf->value("f2MinX1",d_f2MinX1).toFloat());
        setf2MaxX1(cnf->value("f2MaxX1",d_f2MaxX1).toFloat());
        setf2MinY1(cnf->value("f2MinY1",d_f2MinY1).toFloat());
        setf2MaxY1(cnf->value("f2MaxY1",d_f2MaxY1).toFloat());

    }
    else{
        setrMinX1(d_rMinX1);
        setrMaxX1(d_rMaxX1);
        setrMinY1(d_rMinY1);
        setrMaxY1(d_rMaxY1);

        setfMinX1(d_fMinX1);
        setfMaxX1(d_fMaxX1);
        setfMinY1(d_fMinY1);
        setfMaxY1(d_fMaxY1);

        setf2MinX1(d_f2MinX1);
        setf2MaxX1(d_f2MaxX1);
        setf2MinY1(d_f2MinY1);
        setf2MaxY1(d_f2MaxY1);
    }

    saveSettings();
}

qreal ChartViewSettings::rMinX1() const{
    return m_rMinX1;
}

void ChartViewSettings::setrMinX1(qreal rMinX1){

    this->m_rMinX1 = qRound64(rMinX1 * 10) / 10.0;
    emit rMinX1Changed();
    saveTimer.stop();
    saveTimer.start();
}

qreal ChartViewSettings::rMaxX1() const{
    return m_rMaxX1;
}

void ChartViewSettings::setrMaxX1(qreal rMaxX1){

    this->m_rMaxX1 = qRound64(rMaxX1 * 10) / 10.0;
    emit rMaxX1Changed();
    saveTimer.stop();
    saveTimer.start();
}

qreal ChartViewSettings::rMinY1() const{
    return m_rMinY1;
}

void ChartViewSettings::setrMinY1(qreal rMinY1){

    this->m_rMinY1 = qRound64(rMinY1 * 10) / 10.0;
    emit rMinY1Changed();
    saveTimer.stop();
    saveTimer.start();
}

qreal ChartViewSettings::rMaxY1() const{
    return m_rMaxY1;
}

void ChartViewSettings::setrMaxY1(qreal rMaxY1){
    this->m_rMaxY1 = qRound64(rMaxY1 * 10) / 10.0;
    emit rMaxY1Changed();
    saveTimer.stop();
    saveTimer.start();
}

qreal ChartViewSettings::fMinX1() const{
    return m_fMinX1;
}

void ChartViewSettings::setfMinX1(qreal fMinX1){

    this->m_fMinX1 = qRound64(fMinX1 * 10) / 10.0;
    emit fMinX1Changed();
    saveTimer.stop();
    saveTimer.start();
}

qreal ChartViewSettings::fMaxX1() const{
    return m_fMaxX1;
}

void ChartViewSettings::setfMaxX1(qreal fMaxX1){

    this->m_fMaxX1 = qRound64(fMaxX1 * 10) / 10.0;
    emit fMaxX1Changed();
    saveTimer.stop();
    saveTimer.start();
}

qreal ChartViewSettings::fMinY1() const{
    return m_fMinY1;
}

void ChartViewSettings::setfMinY1(qreal fMinY1){

    this->m_fMinY1 = qRound64(fMinY1 * 10) / 10.0;
    emit fMinY1Changed();
    saveTimer.stop();
    saveTimer.start();
}

qreal ChartViewSettings::fMaxY1() const{
    return m_fMaxY1;
}

void ChartViewSettings::setfMaxY1(qreal fMaxY1){
    this->m_fMaxY1 = qRound64(fMaxY1 * 10) / 10.0;
    emit fMaxY1Changed();
    saveTimer.stop();
    saveTimer.start();
}

qreal ChartViewSettings::f2MinX1() const{
    return m_f2MinX1;
}

void ChartViewSettings::setf2MinX1(qreal f2MinX1){

    this->m_f2MinX1 = qRound64(f2MinX1 * 10) / 10.0;
    emit f2MinX1Changed();
    saveTimer.stop();
    saveTimer.start();
}

qreal ChartViewSettings::f2MaxX1() const{
    return m_f2MaxX1;
}

void ChartViewSettings::setf2MaxX1(qreal f2MaxX1){

    this->m_f2MaxX1 = qRound64(f2MaxX1 * 10) / 10.0;
    emit f2MaxX1Changed();
    saveTimer.stop();
    saveTimer.start();
}

qreal ChartViewSettings::f2MinY1() const{
    return m_f2MinY1;
}

void ChartViewSettings::setf2MinY1(qreal f2MinY1){

    this->m_f2MinY1 = qRound64(f2MinY1 * 10) / 10.0;
    emit f2MinY1Changed();
    saveTimer.stop();
    saveTimer.start();
}

qreal ChartViewSettings::f2MaxY1() const{
    return m_f2MaxY1;
}

void ChartViewSettings::setf2MaxY1(qreal f2MaxY1){
    this->m_f2MaxY1 = qRound64(f2MaxY1 * 10) / 10.0;
    emit f2MaxY1Changed();
    saveTimer.stop();
    saveTimer.start();
}

void ChartViewSettings::saveSettings(){

    cnf->setValue("rMinX1",m_rMinX1);
    cnf->setValue("rMaxX1",m_rMaxX1);
    cnf->setValue("rMinY1",m_rMinY1);
    cnf->setValue("rMaxY1",m_rMaxY1);
    cnf->setValue("fMinX1",m_fMinX1);
    cnf->setValue("fMaxX1",m_fMaxX1);
    cnf->setValue("fMinY1",m_fMinY1);
    cnf->setValue("fMaxY1",m_fMaxY1);

    cnf->setValue("f2MinX1",m_f2MinX1);
    cnf->setValue("f2MaxX1",m_f2MaxX1);
    cnf->setValue("f2MinY1",m_f2MinY1);
    cnf->setValue("f2MaxY1",m_f2MaxY1);
}

ChartViewSettings::~ChartViewSettings(){
    saveSettings();
    delete cnf;
}
