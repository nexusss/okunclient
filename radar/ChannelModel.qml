import QtQuick 2.0
import QtCharts 2.1

ListModel {
    property var channel: 0
    property var seriesNumber: 0

    function findChannel(chnum){
        for(var i = 0; i < count; i++){
            if( get(i).channel === chnum)
                return i;
        }
        return -1;
    }

    function enableChannel(state,chnum,name,width,color,axisX1,axisY1,seriesType){
        if(state === false){
            var c0 = findChannel(chnum)

            if(c0 !== -1){
                removeSeries(get(c0).seriesNumber)
                remove(c0)
            }

        }
        else{

            if(seriesType === undefined)
                seriesType = ChartView.SeriesTypeLine

//            var abstractseries = createSeries(ChartView.SeriesTypeLine,name,axisX1,axisY1)

            var abstractseries = createSeries(seriesType,name,axisX1,axisY1)

//            abstractseries.useOpenGL = true;

            if(seriesType === ChartView.SeriesTypeScatter)
                abstractseries.markerSize = width
            else
                abstractseries.width = width
            abstractseries.opacity = 0.5
            abstractseries.color = color
            append({"channel": chnum,"seriesNumber": abstractseries})
            console.log("cnannel model",color,abstractseries.width)
        }
    }
}
