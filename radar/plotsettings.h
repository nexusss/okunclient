#ifndef PLOTSETTINGS_H
#define PLOTSETTINGS_H

#include <QObject>
#include <QSettings>
#include <QTimer>
#include <QDebug>

class PlotSettings : public QObject
{
    Q_OBJECT

    QSettings *cnf;

    QTimer saveTimer;

    Q_PROPERTY(QString backColor READ backColor WRITE setBackColor NOTIFY backColorChanged)
    Q_PROPERTY(QStringList sweepColor READ sweepColor WRITE setSweepColor NOTIFY sweepColorChanged)
    Q_PROPERTY(QStringList channelName READ channelName NOTIFY chanelNameChanged)
    Q_PROPERTY(QString borderColor READ borderColor WRITE setBorderColor NOTIFY borderColorChanged)
    Q_PROPERTY(int sweepWidth READ sweepWidth WRITE setSweepWidth NOTIFY sweepWidthChanged)

    QString m_backColor, m_borderColor;
    int m_sweepWidth;
    QStringList mSweepColor;
    QStringList mchannelName;
public:
    explicit PlotSettings(QObject *parent = 0);
    ~PlotSettings();
    QString backColor() const;
    void setBackColor(QString backColor);
    QStringList sweepColor() const;
    void setSweepColor(QStringList sweepColor);
    QStringList channelName() const;
    QString borderColor() const;
    void setBorderColor(QString borderColor);
    int sweepWidth() const;
    void setSweepWidth(int sweepWidth);

    void saveSettings();
signals:
    void backColorChanged();
    void sweepColorChanged();
    void borderColorChanged();
    void sweepWidthChanged();
    void chanelNameChanged();
};

#endif // PLOTSETTINGS_H
