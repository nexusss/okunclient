#ifndef MYUDPSOCKET_H
#define MYUDPSOCKET_H

#include <QObject>
#include <QUdpSocket>
#include <QVector>
#include <QPointF>
#include "types.h"
class OkunUdpSocket : public QUdpSocket
{
    Q_OBJECT
    quint16 port;

    static const int numofChannels = 8;
    Sweep d[numofChannels];
    QVector <float> sumphase;
    QVector <float> horphase;
    QVector <float> verphase;

    void processingData();
    bool softwareNumber = false;

    float treshold = -100;//порог
    float delta = 0;//разница между уровнями fft для прямого и зеркального

    void procPhaseAndData(const float *sumPhaseArr, const float *horPhaseArr, const float *verPhaseArr, const float *data, const ushort dataSize);

public:
    explicit OkunUdpSocket();

signals:
    void dataReady(Sweep sweep,quint8 channelNumber);    
    void phaseDataReady(QVector<float> phaseDelta,quint8 channelNumber);
    void bindFailed();
    void bindSuccess(quint16 port);

public slots:
    void setPort(quint16 port);    
    void rebind();
    void setTreshold(float treshold);
    void setDelta(float delta);

};

#endif // MYUDPSOCKET_H
