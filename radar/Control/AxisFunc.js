function myfloor(value){
    return Math.ceil(value / 5) * 5
}

function findTickCount(max,min){
    var value = Math.abs(max - min)
    for(var i = 20; i >= 2; i--){
        if(value % i === 0){
            return i + 1
        }
    }
    return  3
}
