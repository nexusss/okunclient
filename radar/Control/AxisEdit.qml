import QtQuick 2.0
import QtQuick.Controls 2.0

import "../../style"
import "AxisFunc.js" as AxisFunc

Item {
    id: item1
    property string axis: ""
    property real max: 0
    property real min: 0
    signal selected(bool state)
    signal minInput(real value)
    signal maxInput(real value)

    implicitWidth: 120
    implicitHeight: 30
    property bool zeroIsMinimum: true

    GroupBox {
        id: groupBox1
        enabled: true
        bottomPadding: 0
        rightPadding: 0
        leftPadding: 0
        topPadding: 12
        spacing: 0
        anchors.fill: parent
        label:  Label {
            id: label1

            text: axis
            anchors.left: parent.left
            anchors.leftMargin: 0
            anchors.right: parent.right
            anchors.rightMargin: 0
            verticalAlignment: Text.AlignVCenter
            horizontalAlignment: Text.AlignHCenter
        }


        background: Rectangle {
            anchors.fill: parent
            color: "transparent"
            border.width: 0
        }


        Row {
            id: row1
            anchors.fill: parent

            Rectangle {
                id: rectangle1
                height: parent.height
                width: parent.width / 2
                color: "#00ffff00"


                TextFieldStyle {
                    id: minAxisField
                    text: min.toFixed(0)
                    onEditingFinished: {
                        text = AxisFunc.myfloor(parseInt(text))
                        minInput(parseInt(text))
                    }
                    validator:IntValidator {top: parseInt(maxAxisField.text) - 1; bottom: zeroIsMinimum ? 0 : Number.NEGATIVE_INFINITY}

                    onActiveFocusChanged: activeFocus ? parent.z = 1 : parent.z = 0

                }
                onZChanged: selected(z > 0 || rectangle2.z > 0)
            }

            Rectangle {
                id: rectangle2
                height: parent.height
                x: parent.width / 2
                width: parent.width / 2
                color: "#00ffff00"


                TextFieldStyle {
                    id: maxAxisField
                    text: max.toFixed(0)
                    onEditingFinished: {
                        text = AxisFunc.myfloor(parseInt(text))
                        maxInput(parseInt(text))
                    }
                    validator: IntValidator {bottom: parseInt(minAxisField.text) + 1}
                    onActiveFocusChanged: activeFocus ? parent.z = 1 : parent.z = 0
                }
                onZChanged: selected(z > 0 || rectangle1.z > 0)
            }
        }
    }
}
