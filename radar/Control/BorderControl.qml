import QtQuick 2.0
import QtQuick.Controls 2.0
import QtQuick.Layouts 1.1

Rectangle {
    id: border
    implicitWidth: 140
    implicitHeight: 60

    property string name: ""
    property real minValue: 0
    property real maxValue: Math.pow(2,16)
    property real value: 0
    color: "#00000000"

    Component.onCompleted: {
        control.value = value
    }

    ColumnLayout {

        anchors.fill: parent

        Text {
            id: text1
            text: name
            font.pointSize: 11
            font.bold: true
            Layout.fillHeight: true
            Layout.fillWidth: true
            verticalAlignment: Text.AlignVCenter
            horizontalAlignment: Text.AlignHCenter
            anchors.left: parent.left
            anchors.leftMargin: 0
            anchors.right: parent.right
            anchors.rightMargin: 0
            font.pixelSize: 12
        }

        SpinBox {
            id: control
            Layout.fillHeight: true
            Layout.fillWidth: true
            anchors.horizontalCenter: parent.horizontalCenter
            editable: true
            from: minValue
            to: maxValue
            value: value
            onValueChanged: border.value = value
            stepSize: 1
//            contentItem: TextField {
//                      z: 2
//                      text: control.textFromValue(control.value, control.locale)

//                      background: Rectangle {
//                                border.color: "#21be2b"
//                                radius: 5
//                            }

//                      font: control.font
//                      color: "#21be2b"

//                      selectionColor: "#21be2b"
//                      selectedTextColor: "#ffffff"
//                      horizontalAlignment: Qt.AlignHCenter
//                      verticalAlignment: Qt.AlignVCenter

//                      readOnly: !control.editable
//                      validator: control.validator
//                      inputMethodHints: Qt.ImhFormattedNumbersOnly
//                  }

                  up.indicator: Rectangle {
                      x: control.mirrored ? 0 : parent.width - width
                      height: parent.height
                      implicitWidth: 40
                      implicitHeight: 40
                      color: control.up.pressed  ? "#353535" : "#828282"

                        radius: 5
                      Text {
                          text: "+"
                          font.pixelSize: control.font.pixelSize * 2
                          color: "#21be2b"
                          anchors.fill: parent
                          fontSizeMode: Text.Fit
                          horizontalAlignment: Text.AlignHCenter
                          verticalAlignment: Text.AlignVCenter
                      }
                  }

                  down.indicator: Rectangle {

                      x: control.mirrored ? parent.width - width : 0
                      height: parent.height
                      implicitWidth: 40
                      implicitHeight: 40
                      color: control.down.pressed ? "#353535" : "#828282"
                        radius: 5
                      Text {
                          text: "-"
                          font.pixelSize: control.font.pixelSize * 2
                          color: "#21be2b"
                          anchors.fill: parent
                          fontSizeMode: Text.Fit
                          horizontalAlignment: Text.AlignHCenter
                          verticalAlignment: Text.AlignVCenter
                      }
                  }

//                  background: Rectangle {
//                      implicitWidth: 140
//                      border.color: "#bdbebf"

//                  }
        }
    }

}
