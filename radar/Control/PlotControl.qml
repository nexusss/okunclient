import QtQuick 2.0
import QtQuick.Layouts 1.1
import QtQuick.Controls 2.0
import "../../style"
Item {
    property int type: 0

    signal zoom()
    signal unzoom()
    signal moveUp()
    signal moveDown()

    implicitWidth: 20
    implicitHeight: 20
    GridLayout {
        id: layout
        flow: (type === 0) ? GridLayout.TopToBottom : GridLayout.LeftToRight
        anchors.fill: parent

        GridLayout{
            flow: (type === 0) ? GridLayout.TopToBottom : GridLayout.LeftToRight
            ButtonStyle{
                id: zoomButton
                Layout.fillHeight: true
                Layout.fillWidth: true
                imgSource: "../radar/icons/Zoom_in.png"
                onClicked: zoom()
            }
            ButtonStyle{
                id: unzoomButton
                Layout.fillHeight: true
                Layout.fillWidth: true
                imgSource: "../radar/icons/Zoom_out.png"
                onClicked: unzoom()
            }
            ButtonStyle{
                id: upButton
                Layout.fillHeight: true
                Layout.fillWidth: true
                imgSource: "../radar/icons/up.png"
                onClicked: moveUp()
            }
            ButtonStyle{
                id: downButton
                Layout.fillHeight: true
                Layout.fillWidth: true
                imgSource: "../radar/icons/down.png"
                onClicked: moveDown()
            }
        }

    }


}
