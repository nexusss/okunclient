import QtQuick 2.0
import QtQuick.Controls 2.0

Control {
    id: parent
    property var currentIndex: 0
    property var currentView: null

    onCurrentIndexChanged: {

        for (var i = 0; i < parent.children.length; i++){
            if(i === currentIndex){
                parent.children[i].visible = true
                currentView = parent.children[i]
            }
            else
                parent.children[i].visible = false
        }
    }

    Component.onCompleted: {

        parent.children[0].visible = true
        currentView = parent.children[0]

        for (var i = 1; i < parent.children.length; i++){
            parent.children[i].visible = false
        }
    }

}
