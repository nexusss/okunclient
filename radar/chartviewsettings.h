#ifndef CHARTVIEWSETTINGS_H
#define CHARTVIEWSETTINGS_H

#include <QObject>
#include <QSettings>
#include <QTimer>

class ChartViewSettings : public QObject
{
    Q_OBJECT
    QSettings *cnf;


    QTimer saveTimer;

    Q_PROPERTY(qreal rMinX1 READ rMinX1 WRITE setrMinX1 NOTIFY rMinX1Changed)
    Q_PROPERTY(qreal rMaxX1 READ rMaxX1 WRITE setrMaxX1 NOTIFY rMaxX1Changed)
    Q_PROPERTY(qreal rMinY1 READ rMinY1 WRITE setrMinY1 NOTIFY rMinY1Changed)
    Q_PROPERTY(qreal rMaxY1 READ rMaxY1 WRITE setrMaxY1 NOTIFY rMaxY1Changed)

    Q_PROPERTY(qreal fMinX1 READ fMinX1 WRITE setfMinX1 NOTIFY fMinX1Changed)
    Q_PROPERTY(qreal fMaxX1 READ fMaxX1 WRITE setfMaxX1 NOTIFY fMaxX1Changed)
    Q_PROPERTY(qreal fMinY1 READ fMinY1 WRITE setfMinY1 NOTIFY fMinY1Changed)
    Q_PROPERTY(qreal fMaxY1 READ fMaxY1 WRITE setfMaxY1 NOTIFY fMaxY1Changed)

    Q_PROPERTY(qreal f2MinX1 READ f2MinX1 WRITE setf2MinX1 NOTIFY f2MinX1Changed)
    Q_PROPERTY(qreal f2MaxX1 READ f2MaxX1 WRITE setf2MaxX1 NOTIFY f2MaxX1Changed)
    Q_PROPERTY(qreal f2MinY1 READ f2MinY1 WRITE setf2MinY1 NOTIFY f2MinY1Changed)
    Q_PROPERTY(qreal f2MaxY1 READ f2MaxY1 WRITE setf2MaxY1 NOTIFY f2MaxY1Changed)

    qreal m_rMinX1,m_rMaxX1,m_rMinY1,m_rMaxY1;
    qreal m_fMinX1,m_fMaxX1,m_fMinY1,m_fMaxY1;
    qreal m_f2MinX1,m_f2MaxX1,m_f2MinY1,m_f2MaxY1;

public:
    explicit ChartViewSettings(QObject *parent = 0);
    ~ChartViewSettings();
    qreal rMinX1() const;
    void setrMinX1(qreal rMinX1);
    qreal rMaxX1() const;
    void setrMaxX1(qreal rMaxX1);
    qreal rMinY1() const;
    void setrMinY1(qreal rMinY1);
    qreal rMaxY1() const;
    void setrMaxY1(qreal rMaxY1);

    qreal fMinX1() const;
    void setfMinX1(qreal fMinX1);
    qreal fMaxX1() const;
    void setfMaxX1(qreal fMaxX1);
    qreal fMinY1() const;
    void setfMinY1(qreal fMinY1);
    qreal fMaxY1() const;
    void setfMaxY1(qreal fMaxY1);

    qreal f2MinX1() const;
    void setf2MinX1(qreal f2MinX1);
    qreal f2MaxX1() const;
    void setf2MaxX1(qreal f2MaxX1);
    qreal f2MinY1() const;
    void setf2MinY1(qreal f2MinY1);
    qreal f2MaxY1() const;
    void setf2MaxY1(qreal f2MaxY1);

    void saveSettings();
    Q_INVOKABLE void initialize(QString settingsFileName);
signals:
    void rMinX1Changed();
    void rMaxX1Changed();
    void rMinY1Changed();
    void rMaxY1Changed();

    void fMinX1Changed();
    void fMaxX1Changed();
    void fMinY1Changed();
    void fMaxY1Changed();

    void f2MinX1Changed();
    void f2MaxX1Changed();
    void f2MinY1Changed();
    void f2MaxY1Changed();
public slots:
};

#endif // CHARTVIEWSETTINGS_H
