#ifndef PERCHDATA_H
#define PERCHDATA_H

#include <QObject>
#include <QtCharts/QAbstractSeries>
#include <QThread>
#include <QAbstractBarSeries>
#include <QBarSet>
#include <QBarSeries>

#include "okunudpsocket.h"
#include "types.h"


typedef QVector<QPointF> Pointf;

QT_CHARTS_USE_NAMESPACE


class PerchData : public QObject
{
    Q_OBJECT
    Q_PROPERTY(quint16 channelCount READ channelCount WRITE setChannelCount )
    quint16 mchannelCount = 8;


    static const auto maxNumChannels = 8;

    float harmWeight = 1;
    QVector <Pointf> sweep;

    QVector <Pointf> phase;

    OkunUdpSocket *okunSocket;
    QThread udpThread;

public:
    explicit PerchData(QObject *parent = 0);

    ~PerchData();
    quint16 channelCount();
    enum class LINETYPE{STICK,LINE};
    Q_INVOKABLE LINETYPE getLineType();

signals:
    void startBind(quint16 port);
public slots:

    void setChannelCount(quint16 channel);
    void setLineType(LINETYPE linetype);
    void setData(Sweep sweep,quint8 channelNumber);
    void updateData(QAbstractSeries *series, const quint8 channelNumber);
    void updatePhase(QAbstractSeries *series, const quint8 channelNumber);
    void setPhase(QVector<float> sweep, quint8 type);
    void setTreshold(float treshold);
    void setDelta(float delta);
private:
    LINETYPE linetype = LINETYPE::STICK;
    Q_ENUMS(LINETYPE)
};

#endif // PERCHDATA_H
