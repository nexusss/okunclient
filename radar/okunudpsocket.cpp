#include "okunudpsocket.h"

#pragma pack(push, 1)
struct PacketStruct1{
    unsigned short dataSize;
    short data[4096];
}packetStruct1;
struct PacketStruct0{
    unsigned short dataSize;
    short data[2048];
}packetStruct0;
#pragma pack(pop)
#pragma pack(push, 1)
struct PacketStruct2{
    unsigned short dataReadNumber;
    unsigned short dataSize;
    float data[256];
    float sumPhase[256];
    float horPhase[256];
    float verPhase[256];
}packetStruct2;
struct PacketStruct3{
    unsigned short dataReadNumber;
    unsigned short dataSize;
    float data[512];
    float sumPhase[512];
    float horPhase[512];
    float verPhase[512];
}packetStruct3;
#pragma pack(pop)
#pragma pack(push, 1)
struct PacketStruct4{
    unsigned short dataSize;
    float chL[256];
    float chU[256];
    float chR[256];
    float chD[256];
}packetStruct4;
struct PacketStruct5{
    unsigned short dataSize;
    float chL[512];
    float chU[512];
    float chR[512];
    float chD[512];
}packetStruct5;
#pragma pack(pop)

OkunUdpSocket::OkunUdpSocket() : port(0)
{
    connect(this,&QUdpSocket::readyRead,this,&OkunUdpSocket::processingData);
}

void OkunUdpSocket::procPhaseAndData(const float *sumPhaseArr, const float *horPhaseArr, const float *verPhaseArr,
                                     const float *data, const ushort dataSize){

    bool showPhase[dataSize];

    for(int i = 0; i < dataSize; i++)
        showPhase[i] = true;

    float maxDirect = -200;
    float maxMirror = -200;

    for(int i = 0, k = dataSize - 1; i < dataSize / 2 ; i++, k--){
        d[0].push_back(data[i] );
        d[1].push_back(data[k] );

        if(data[i] < treshold){
            showPhase[i] = false;
        }

        if(data[k] < treshold){
            showPhase[k] = false;
        }

        if(fabsf(data[i] - data[k]) < delta){
            showPhase[i] = false;
            showPhase[k] = false;
        }

        //search max
        if(maxDirect < data[i])
            maxDirect = data[i] ;

        if(maxMirror < data[k])
            maxMirror = data[k] ;

    }

    if(maxDirect > maxMirror){
        for(unsigned short i = 0; i < dataSize / 2; i++){
            if(showPhase[i] == true){
                sumphase.push_back(sumPhaseArr[i]);
                horphase.push_back(horPhaseArr[i]);
                verphase.push_back(verPhaseArr[i]);
            }
            else{
                sumphase.push_back(-200);
                horphase.push_back(-200);
                verphase.push_back(-200);
            }
        }
    }
    else{
        for(unsigned short i = dataSize - 1; i >= dataSize / 2; i--){
            if(showPhase[i] == true){
                sumphase.push_back(sumPhaseArr[i]);
                horphase.push_back(horPhaseArr[i]);
                verphase.push_back(verPhaseArr[i]);
            }
            else{
                sumphase.push_back(-200);
                horphase.push_back(-200);
                verphase.push_back(-200);
            }
        }
    }

    emit phaseDataReady(sumphase,0);
    emit phaseDataReady(horphase,1);
    emit phaseDataReady(verphase,2);

    d[1].push_back(-100);

    sumphase.push_back(-200);
    horphase.push_back(-200);
    verphase.push_back(-200);
}

void OkunUdpSocket::processingData(){

    while (hasPendingDatagrams()) {

        quint16 size = pendingDatagramSize();

        char buf[size];
        readDatagram((char *)&buf, pendingDatagramSize());

        switch(buf[0]){
            case 0:{

                memcpy(&packetStruct0,buf + 1,sizeof(packetStruct0));

                for(int i = 0; i < packetStruct0.dataSize; i = i + 8){

                    d[0].push_back(packetStruct0.data[i]);
                    d[1].push_back(packetStruct0.data[i + 1]);
                    d[2].push_back(packetStruct0.data[i + 2]);
                    d[3].push_back(packetStruct0.data[i + 3]);
                    d[4].push_back(packetStruct0.data[i + 4]);
                    d[5].push_back(packetStruct0.data[i + 5]);
                    d[6].push_back(packetStruct0.data[i + 6]);
                    d[7].push_back(packetStruct0.data[i + 7]);
                }
                break;
            }

            case 1:{
                memcpy(&packetStruct1,buf + 1,sizeof(packetStruct1));

                for(int i = 0; i < packetStruct1.dataSize ; i = i + 8){

                    d[0].push_back(packetStruct1.data[i]);
                    d[1].push_back(packetStruct1.data[i + 1]);
                    d[2].push_back(packetStruct1.data[i + 2]);
                    d[3].push_back(packetStruct1.data[i + 3]);
                    d[4].push_back(packetStruct1.data[i + 4]);
                    d[5].push_back(packetStruct1.data[i + 5]);
                    d[6].push_back(packetStruct1.data[i + 6]);
                    d[7].push_back(packetStruct1.data[i + 7]);
                }

                break;
            }
            case 2:{
            sumphase.clear();
            horphase.clear();
            verphase.clear();
                memcpy(&packetStruct2,buf + 1,sizeof(packetStruct2));

                qDebug() << "packetStruct2.dataReadNumber" << packetStruct2.data[1] << packetStruct2.data[2];

                procPhaseAndData(packetStruct2.sumPhase,packetStruct2.horPhase, packetStruct2.verPhase,packetStruct2.data,packetStruct2.dataSize);
                break;
            }
            case 3:{
                memcpy(&packetStruct3,buf + 1,sizeof(packetStruct3));

                qDebug() << "packetStruct3.dataReadNumber" << packetStruct3.dataReadNumber;

                sumphase.clear();
                horphase.clear();
                verphase.clear();

                procPhaseAndData(packetStruct3.sumPhase,packetStruct3.horPhase, packetStruct3.verPhase,packetStruct3.data,packetStruct3.dataSize / 2);
                procPhaseAndData(packetStruct3.sumPhase + 256,packetStruct3.horPhase + 256, packetStruct3.verPhase + 256,packetStruct3.data + 256,packetStruct3.dataSize / 2);

                break;
            }
            case 4:{
                memcpy(&packetStruct4,buf + 1,sizeof(packetStruct4));

                for(int i = 0; i < packetStruct4.dataSize / 2; i++){
                    d[0].push_back(packetStruct4.chL[i]);
                    d[2].push_back(packetStruct4.chU[i]);
                    d[4].push_back(packetStruct4.chR[i]);
                    d[6].push_back(packetStruct4.chD[i]);
                }

                d[1].push_back(-100);
                d[3].push_back(-100);
                d[5].push_back(-100);
                d[7].push_back(-100);

                for(int i = packetStruct4.dataSize -1; i >= packetStruct4.dataSize / 2; i--){
                    d[1].push_back(packetStruct4.chL[i]);
                    d[3].push_back(packetStruct4.chU[i]);
                    d[5].push_back(packetStruct4.chR[i]);
                    d[7].push_back(packetStruct4.chD[i]);
                }

                break;
            }

            case 5:{
                memcpy(&packetStruct5,buf + 1,sizeof(packetStruct5));
                unsigned short  size = packetStruct5.dataSize / 4.0;


                for(int i = 0; i < size; i++){
                    d[0].push_back(packetStruct5.chL[i]);
                    d[2].push_back(packetStruct5.chU[i]);
                    d[4].push_back(packetStruct5.chR[i]);
                    d[6].push_back(packetStruct5.chD[i]);
                }

                d[1].push_back(-100);
                d[3].push_back(-100);
                d[5].push_back(-100);
                d[7].push_back(-100);

                for(int i = 255, k = 0; k < size - 1; i--, k++){
                    d[1].push_back(packetStruct5.chL[i]);
                    d[3].push_back(packetStruct5.chU[i]);
                    d[5].push_back(packetStruct5.chR[i]);
                    d[7].push_back(packetStruct5.chD[i]);
                }

                for(int i = 256, k = 0; k < size; i++, k++){
                    d[0].push_back(packetStruct5.chL[i]);
                    d[2].push_back(packetStruct5.chU[i]);
                    d[4].push_back(packetStruct5.chR[i]);
                    d[6].push_back(packetStruct5.chD[i]);
                }

                d[1].push_back(-100);
                d[3].push_back(-100);
                d[5].push_back(-100);
                d[7].push_back(-100);

                for(int i = 511, k = 0; k < size - 1; i--, k++){
                    d[1].push_back(packetStruct5.chL[i]);
                    d[3].push_back(packetStruct5.chU[i]);
                    d[5].push_back(packetStruct5.chR[i]);
                    d[7].push_back(packetStruct5.chD[i]);
                }

                break;
            }

        }

        for(int i = 0; i < numofChannels; i++){
                emit dataReady(d[i],i);
                d[i].clear();
        }


    }
}

void OkunUdpSocket::setPort(quint16 port){
    if(this->port != port)
        this->port = port;
}

void OkunUdpSocket::rebind(){
    qDebug() << "rebind" << port;

    if(port > 0){
        abort();
qDebug() << "abort";
         if(!bind(port)){
             emit bindFailed();
//             qDebug() << "bind fail" << port;
         }
         else{
//             qDebug() << "bind succ" << port;
             emit bindSuccess(port);
         }
          qDebug() << "abort";
    }
    qDebug() << "end rebind";
}

void OkunUdpSocket::setTreshold(float treshold){
    this->treshold = treshold;
    qDebug() << "set treshold" << treshold;
}

void OkunUdpSocket::setDelta(float delta){
    this->delta = delta;
    qDebug() << "set delta" << delta;
}
