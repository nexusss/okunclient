import QtQuick 2.0
import QtQuick.Controls 2.0

Rectangle {
    id: page
    border.width: 4
    border.color: "white"

    anchors.fill: parent
    property var colorList: ""
    property var modelList: ""
    signal checkedChanged(bool state,int chnum)

    Row {
     id: legendRowFirst

     anchors.centerIn: parent
     spacing: 1

     Repeater {
         id: legendRepeater
         model: modelList
         delegate: legendDelegate
     }

     Component {
         id: legendDelegate
         CheckBox{
             id: control
             text: modelList[index]
             checked: true

                   contentItem: Text {
                       text: control.text
                       font: control.font
                       opacity: enabled ? 1.0 : 0.3
                       color:  "black"
                       horizontalAlignment: Text.AlignLeft
                       verticalAlignment: Text.AlignVCenter
                   }

                   indicator: Rectangle {
                       implicitWidth: 20
                       implicitHeight: 20
                       x: control.width - width - control.rightPadding
                       y: control.topPadding + control.availableHeight / 2 - height / 2
                       radius: 3
                       color: "transparent"
                       border.color: sweepColorList[index]

                       Rectangle {
                           width: 10
                           height: 10
                           x: 5
                           y: 5
                           radius: 0
                           color:  sweepColorList[index]
                           visible: control.checked
                       }
                   }

                   background: Rectangle {
                       implicitWidth: 70
                       implicitHeight: 20
                       radius: 4
                       color: "#bdbebf"
                   }
             onCheckStateChanged: page.checkedChanged(checked,index)
         }
     }
    }
}
