import QtQuick 2.0
import QtQuick.Controls 2.0

Item {
    property var sweepColorList: ""
    property var channelNameList: ""
    property var fftNameList: ["direct","mirror"]
    property var secondNameList: ["left D","left M","up D","up M","right D","right M","down D","down M"]
    implicitHeight: 30
    implicitWidth: 70
    signal disableEnableChannel(bool state,int chnum)
    function setSoftwareNumber(softwarenumber){

        legendStack.currentIndex = softwarenumber
    }

    StackedView{
        id: legendStack
        anchors.fill: parent
        LegendPage{

            anchors.centerIn: parent
            colorList: sweepColorList
            modelList: channelNameList
            onCheckedChanged: disableEnableChannel(state,chnum)
        }

        LegendPage{

            anchors.centerIn: parent
            colorList: sweepColorList
            modelList: fftNameList            
            onCheckedChanged: disableEnableChannel(state,chnum)
        }
        LegendPage{

            anchors.centerIn: parent
            colorList: sweepColorList
            modelList: secondNameList

            onCheckedChanged: disableEnableChannel(state,chnum)
        }
    }

}
