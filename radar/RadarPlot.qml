import QtQuick 2.7
import QtQuick.Layouts 1.1
import QtQuick.Controls 2.0

import dokltd.plotSettings 1.0
import dokltd.chartViewSettings 1.0
import olvia.perchData 1.0
import "Control"

Item {

    id: rPlot
    property string backColor: plotSettings.backColor

    property string borderColor: plotSettings.borderColor
    property int sweepWidth: plotSettings.sweepWidth

    property bool border: false

    signal startBind(int port)

    onBackColorChanged: plotSettings.backColor = backColor

    onBorderColorChanged: plotSettings.borderColor = borderColor
    onSweepWidthChanged: plotSettings.sweepWidth = sweepWidth

    function setTreshold(treshold){
        pData.setTreshold(treshold)
    }

    function setDelta(delta){
        pData.setDelta(delta)
    }

    PerchData{
        id: pData
        onStartBind: {
            rPlot.startBind(port)
        }

        Component.onCompleted: {
            setLineType(PerchData.LINE)
        }

    }

    PlotSettings{
        id: plotSettings
    }

    function setSoftwareNumber(softnumber){

        chartswipe.currentIndex = softnumber
        myLegend.setSoftwareNumber(softnumber)
    }

    ChartViewSettings{
        id: chartViewSettings

        Component.onCompleted: {
            initialize("chart")
            rChart.initAxis(rMinY1,rMaxY1, rMinX1, rMaxX1)
            rChartFirst.initAxis(fMinY1,fMaxY1, fMinX1, fMaxX1)
            secondChartView.initAxis(f2MinY1,f2MaxY1, f2MinX1, f2MaxX1)

//            rChartFirst.currentAxis = {"minAxisY1": fMinY1,"maxAxisY1": fMaxY1,"minAxisX1": fMinX1,"maxAxisX1": fMaxX1}
//            secondChartView.currentAxis = {"minAxisY1": f2MinY1,"maxAxisY1": f2MaxY1,"minAxisX1": f2MinX1,"maxAxisX1": f2MaxX1}

        }
    }

    GridLayout{
        anchors.fill: parent
        columns: 2
        rowSpacing: 0
        /*PlotControl{
            id: yControl
            Layout.fillHeight: true
            onZoom: chartswipe.currentView.zoomY1()
            onUnzoom: chartswipe.currentView.unzoomY1()
            onMoveUp: chartswipe.currentView.upY1()
            onMoveDown: chartswipe.currentView.downY1()
        }*/


        ColumnLayout{
            spacing: 2
            Layout.fillHeight: true
            Layout.fillWidth: true
            Layout.row: 0
            Layout.column: 1
                StackedView{
                    id: chartswipe
                    Layout.fillHeight: true
                    Layout.fillWidth: true

                    RawChartView {
                        id: rChart

                        perchData: pData

                        anchors.fill: parent
                        sweepColorList: plotSettings.sweepColor

                        channelNameList: plotSettings.channelName

                        border: rPlot.border
                        backgroundColor: backColor

                        lineSeriesWidth: sweepWidth
                        lineBorderColor: borderColor                        

                        Component.onDestruction: {

                            chartViewSettings.rMaxX1 = currentAxis.maxAxisX1
                            chartViewSettings.rMinX1 = currentAxis.minAxisX1
                            chartViewSettings.rMaxY1 = currentAxis.maxAxisY1
                            chartViewSettings.rMinY1 = currentAxis.minAxisY1
                        }
                        onVisibleChanged: {
                            if(visible)
                                pData.setLineType(PerchData.LINE)
                        }

                    }
                    FirstChartView {
                        id: rChartFirst

                        perchData: pData
                        anchors.fill: parent

                        sweepColorList: plotSettings.sweepColor

                        channelNameList: plotSettings.channelName

                        border: rPlot.border
                        backgroundColor: backColor

                        lineSeriesWidth: sweepWidth
                        lineBorderColor: borderColor


                        Component.onDestruction: {


                            chartViewSettings.fMaxX1 = currentAxis.maxAxisX1
                            chartViewSettings.fMinX1 = currentAxis.minAxisX1
                            chartViewSettings.fMaxY1 = currentAxis.maxAxisY1
                            chartViewSettings.fMinY1 = currentAxis.minAxisY1
                        }

                        onVisibleChanged: {
                            if(visible)
                                pData.setLineType(PerchData.STICK)
                        }
                    }

                    SecondChartView {
                        id: secondChartView

                        perchData: pData
                        anchors.fill: parent

                        sweepColorList: plotSettings.sweepColor

                        channelNameList: plotSettings.channelName

                        border: rPlot.border
                        backgroundColor: backColor

                        lineSeriesWidth: sweepWidth
                        lineBorderColor: borderColor


                        Component.onDestruction: {


                            chartViewSettings.f2MaxX1 = currentAxis.maxAxisX1
                            chartViewSettings.f2MinX1 = currentAxis.minAxisX1
                            chartViewSettings.f2MaxY1 = currentAxis.maxAxisY1
                            chartViewSettings.f2MinY1 = currentAxis.minAxisY1
                       }

                        onVisibleChanged: {
                            if(visible)
                                pData.setLineType(PerchData.STICK)
                        }
                    }
                }
                CustomLegend{
                    id: myLegend
                    Layout.alignment: Qt.AlignHCenter | Qt.AlignVCenter

                    Layout.fillWidth: true
                    sweepColorList: plotSettings.sweepColor
                    channelNameList: plotSettings.channelName
                    onDisableEnableChannel: chartswipe.currentView.enableChannel(state,chnum)
                }
        }

        /*PlotControl{
            id: xControl
            type: 1
            Layout.row: 1
            Layout.column: 1
            Layout.fillWidth: true

            onZoom: chartswipe.currentView.zoomX()
            onUnzoom: chartswipe.currentView.unzoomX()
            onMoveUp: chartswipe.currentView.leftX()
            onMoveDown: chartswipe.currentView.rightX()
        }*/

    }
    Shortcut{
        sequence: "Right"
        onActivated: chartswipe.currentView.rightX()
    }
    Shortcut{
        sequence: "Left"
        onActivated: chartswipe.currentView.leftX()
    }
    Shortcut{
        sequence: "Up"
        onActivated: chartswipe.currentView.upY1()
    }

    Shortcut{
        sequence: "Down"
        onActivated: chartswipe.currentView.downY1()
    }
    Shortcut{
        sequence: "Ctrl+Right"
        onActivated: chartswipe.currentView.zoomX()
    }
    Shortcut{
        sequence: "Ctrl+Left"
        onActivated: chartswipe.currentView.unzoomX()
    }
    Shortcut{
        sequence: "Ctrl+Up"
        onActivated: chartswipe.currentView.zoomY1()
    }
    Shortcut{
        sequence: "Ctrl+Down"
        onActivated: chartswipe.currentView.unzoomY1()
    }
}
