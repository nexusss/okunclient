import QtQuick 2.7
import QtCharts 2.1

AbstractRadarChartView {
    id: chartView


    ChannelModel {
          id: channelModel
    }

    ChannelModel {
          id: phaseModel
    }

    xzeroIsMinimum: false

    Component.onCompleted: {
        lineSeriesWidth = 5
        refreshTimer.triggered.connect(function(){

            for(var i = 0; i < channelModel.count; i++){
//              for(var i = 0; i < 3; i++){
                  var ser = phaseModel.get(i).seriesNumber;
//                  console.log("trig",phaseModel.count,phaseModel.get(i).seriesNumber,ser.color);
                perchData.updateData(channelModel.get(i).seriesNumber,channelModel.get(i).channel)
//                perchData.updateData(phaseModel.get(i).seriesNumber,phaseModel.get(i).channel)
//                  perchData.updatePhase(channelModel.get(i).seriesNumber,phaseModel.get(i).channel)
                perchData.updatePhase(phaseModel.get(i).seriesNumber,phaseModel.get(i).channel)
//                console.log("ch",i,phaseModel.get(i).channel);
            }
        })



//        console.log("channel count",channelModel.count)
            for(var i = 0; i < perchData.channelCount
                        && i < channelNameList.length
                        && i < sweepColorList.length; i++){
                channelModel.enableChannel(true,i,channelNameList[i],lineSeriesWidth,sweepColorList[i],xAxis,yAxis)
                phaseModel.enableChannel(true,i,channelNameList[i],lineSeriesWidth + 2,sweepColorList[i],xAxis,yAxis,ChartView.SeriesTypeScatter)
            }

//            console.log("channel count",channelModel.count)
    }

    function enableChannel(state,chnum){

        if(state === true){
            channelModel.enableChannel(true,chnum,channelNameList[chnum],lineSeriesWidth,sweepColorList[chnum],xAxis,yAxis)
//            phaseModel.enableChannel(true,chnum,channelNameList[chnum],lineSeriesWidth,sweepColorList[chnum],xAxis,yAxis)
        }
        else{
            channelModel.enableChannel(false,chnum,null,null,null,null,null)
//            phaseModel.enableChannel(false,chnum,null,null,null,null,null)
        }
    }
}

