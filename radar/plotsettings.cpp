#include "plotsettings.h"
#include <QDir>
PlotSettings::PlotSettings(QObject *parent) : QObject(parent)
{

    saveTimer.setSingleShot(true);
    saveTimer.setInterval(1 * 1000);

    connect(&saveTimer,&QTimer::timeout,this,&PlotSettings::saveSettings);

    QString settingsPath = QDir::homePath() + "/okunClient";

    QString d_backColor = "darkGray",d_borderColor = "gray";
    QStringList dSweepColor = {"red","green","blue","orange","yellow","cyan","magenta","brown"},
            dChannelName = {"ch 1","ch 2","ch 3","ch 4","ch 5","ch 6","ch 7","ch 8"};

    int d_sweepWidth = 3;

    QDir settingsDir;
    settingsDir.mkdir(settingsPath);

    cnf = new QSettings(settingsPath + "/plot.ini", QSettings::IniFormat);

        setBackColor(cnf->value("backColor",d_backColor).toString());
        setSweepColor(cnf->value("sweepColor",dSweepColor).toStringList());
        mchannelName = cnf->value("channelName",dChannelName).toStringList();
        setBorderColor(cnf->value("borderColor",d_borderColor).toString());
        setSweepWidth(cnf->value("sweepWidth",d_sweepWidth).toInt());


    saveSettings();
}


QString PlotSettings::backColor() const{
    return m_backColor;
}

void PlotSettings::setBackColor(QString backColor){
    this->m_backColor = backColor;
    saveTimer.stop();
    saveTimer.start();
}

QStringList PlotSettings::sweepColor() const{
    return mSweepColor;
}

void PlotSettings::setSweepColor(QStringList sweepColor){
    if(this->mSweepColor != sweepColor){
        this->mSweepColor = sweepColor;
        emit sweepColorChanged();
        saveTimer.stop();
        saveTimer.start();
    }
}

QStringList PlotSettings::channelName() const{
    return mchannelName;
}

QString PlotSettings::borderColor() const{
    return m_borderColor;
}

void PlotSettings::setBorderColor(QString borderColor){
    m_borderColor = borderColor;
    saveTimer.stop();
    saveTimer.start();
}

int PlotSettings::sweepWidth() const{
    return m_sweepWidth;
}

void PlotSettings::setSweepWidth(int sweepWidth){
    m_sweepWidth = sweepWidth;
    saveTimer.stop();
    saveTimer.start();
}

void PlotSettings::saveSettings(){

    cnf->setValue("backColor",m_backColor);
    cnf->setValue("sweepColor",mSweepColor);
    cnf->setValue("borderColor",m_borderColor);
    cnf->setValue("sweepWidth",m_sweepWidth);
    qDebug() << "save";
}

PlotSettings::~PlotSettings(){
    delete cnf;
}

