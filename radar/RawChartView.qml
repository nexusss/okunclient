import QtQuick 2.7
import QtCharts 2.1


AbstractRadarChartView {
    id: chartView


    ChannelModel {
          id: channelModel
    }

    Component.onCompleted: {
        refreshTimer.triggered.connect(function(){
            for(var i = 0; i < channelModel.count; i++)
                perchData.updateData(channelModel.get(i).seriesNumber,channelModel.get(i).channel)
        })



        console.log("channel count",channelModel.count)
            for(var i = 0; i < perchData.channelCount
                        && i < channelNameList.length
                        && i < sweepColorList.length; i++){
                channelModel.enableChannel(true,i,channelNameList[i],lineSeriesWidth,sweepColorList[i],xAxis,yAxis)
            }

            console.log("channel count",channelModel.count)
    }

    function enableChannel(state,chnum){
        if(state === true){
            channelModel.enableChannel(true,chnum,channelNameList[chnum],lineSeriesWidth,sweepColorList[chnum],xAxis,yAxis)
        }
        else{
            channelModel.enableChannel(false,chnum,null,null,null,null,null)
        }
    }
}
