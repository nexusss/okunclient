import QtQuick 2.5
import QtQuick.Window 2.0
import QtQuick.Dialogs 1.2
import QtQuick.Controls 2.0
import QtQuick.Controls.Material 2.0
import QtQuick.Layouts 1.1

Window {
    id: root
    width: 300
    height: 280

    property string backColor: ""
    property string ch0Color: ""
    property string ch1Color: ""
    property string ch2Color: ""
    property string ch3Color: ""
    property string ch4Color: ""
    property string ch5Color: ""
    property string ch6Color: ""
    property string ch7Color: ""
    property string borderColor: ""
    property int sweepWidth: 2

    ColumnLayout{
        anchors.fill: parent
        ColorSetings{
            labelText: "background"
            colorChoose: backColor
            width: root.width
            onColorChooseChanged: backColor = colorChoose
        }
        ColorSetings{
            labelText: "ch 0"
            colorChoose: ch0Color
            width: root.width
            onColorChooseChanged: ch0Color = colorChoose
        }
        ColorSetings{
            labelText: "ch 1"
            colorChoose: ch1Color
            width: root.width
            onColorChooseChanged: ch1Color = colorChoose
        }
        ColorSetings{
            labelText: "ch 2"
            colorChoose: ch2Color
            width: root.width
            onColorChooseChanged: ch2Color = colorChoose
        }
        ColorSetings{
            labelText: "ch 3"
            colorChoose: ch3Color
            width: root.width
            onColorChooseChanged: ch3Color = colorChoose
        }
        ColorSetings{
            labelText: "ch 4"
            colorChoose: ch4Color
            width: root.width
            onColorChooseChanged: ch4Color = colorChoose
        }
        ColorSetings{
            labelText: "ch 5"
            colorChoose: ch5Color
            width: root.width
            onColorChooseChanged: ch5Color = colorChoose
        }
        ColorSetings{
            labelText: "ch 6"
            colorChoose: ch6Color
            width: root.width
            onColorChooseChanged: ch6Color = colorChoose
        }
        ColorSetings{
            labelText: "ch 7"
            colorChoose: ch7Color
            width: root.width
            onColorChooseChanged: ch7Color = colorChoose
        }
        ColorSetings{
            labelText: "border"
            colorChoose: borderColor
            width: root.width
            onColorChooseChanged: borderColor = colorChoose
        }

        RowLayout{
            Label{
                text: "border width"
                font.pointSize: 16
                Layout.minimumWidth: 150
            }

            SpinBox {
                from: 1
                to: 99
                value: sweepWidth < 1 ? 1 : sweepWidth
                onValueChanged: sweepWidth = value
            }
        }
    }

}
