#include "perchdata.h"
#include <QXYSeries>
#include <QDateTime>


PerchData::PerchData(QObject *parent) : QObject(parent)
{
qDebug() << "perch data";

    qRegisterMetaType<Sweep>("Sweep");
    okunSocket = new OkunUdpSocket();

    okunSocket->moveToThread(&udpThread);
connect(okunSocket,&OkunUdpSocket::bindSuccess,this,&PerchData::startBind);
connect(okunSocket,&OkunUdpSocket::bindSuccess,[=](){
    qDebug( ) << "bind success";
});

    connect(okunSocket,&OkunUdpSocket::bindFailed,[=](){
        qDebug() << "bind failed0";
        int port = qrand() % 2000 + 2000;
        qDebug() << "bind failed1";
        okunSocket->setPort(port);
        qDebug() << "bind failed2";
        okunSocket->rebind();
        qDebug() << "bind failed3";
    });


    connect(okunSocket,&OkunUdpSocket::dataReady,this,&PerchData::setData);
    connect(okunSocket,&OkunUdpSocket::phaseDataReady,this,&PerchData::setPhase);
    connect(&udpThread,&QThread::started,[=](){
qDebug() << "rebind start0";
        qsrand(QTime::currentTime().msec());
        qDebug() << "rebind start1";
        int port = 57600;
        qDebug() << "rebind start2";
        okunSocket->setPort(port);
        qDebug() << "rebind start3";
        okunSocket->rebind();
        qDebug() << "rebind start";
    });
    connect(&udpThread,&QThread::finished,okunSocket,&OkunUdpSocket::deleteLater);


    sweep.resize(mchannelCount);
    phase.resize(mchannelCount);

   /* QPointF pointf;
    pointf.setX(56);
    pointf.setY(0);

    phase[0].push_back(pointf);
    pointf.setX(85);
    phase[1].push_back(pointf);
    pointf.setX(95);
    phase[2].push_back(pointf);


    pointf.setX(56);
    pointf.setY(0);

    sweep[0].push_back(pointf);
    pointf.setX(85);
    sweep[0].push_back(pointf);
    pointf.setX(95);
    sweep[0].push_back(pointf);*/

    udpThread.start();
}

quint16 PerchData::channelCount(){
    return mchannelCount;
}

PerchData::LINETYPE PerchData::getLineType(){
    return linetype;
}

void PerchData::setData(Sweep sweep, quint8 channelNumber){

    if( channelNumber < mchannelCount){
        this->sweep[channelNumber].clear();


            if(linetype == LINETYPE::STICK){

                for( int i = 0; i < sweep.size(); i++) {
                    QPointF harm;
                    harm.setX(i * harmWeight);
                    harm.setY(-100);
                    this->sweep[channelNumber].push_back(harm);

                    QPointF harm2;
                    harm2.setX(i * harmWeight);
                    harm2.setY(sweep.at(i ));
                    this->sweep[channelNumber].push_back(harm2);

                    QPointF harm3;
                    harm3.setX(i * harmWeight);
                    harm3.setY(-100);
                    this->sweep[channelNumber].push_back(harm3);
                }
            }
            else{
                for( int i = 0; i < sweep.size(); i++) {
                    QPointF harm;
                    harm.setX(i);
                    harm.setY(sweep.at(i ));
                    this->sweep[channelNumber].push_back(harm);
                }
            }
    }
}

void PerchData::updateData(QAbstractSeries *series, const quint8 channelNumber){


    if (series  && channelNumber < mchannelCount) {

        QXYSeries *xySeries = static_cast<QXYSeries *>(series);
        // Use replace instead of clear + append, it's optimized for performance
//        for(int i = 0; i < sweep[sweepNumber][channelNumber].size(); i++){
//            qDebug() << channelNumber << sweep[sweepNumber][channelNumber].at(i);
//        }

//        qDebug() << "replace" << channelNumber << "sw" << sweep[channelNumber];
        xySeries->replace(sweep[channelNumber]);
//        xySeries->replace(phase[channelNumber]);

    }
}

void PerchData::updatePhase(QAbstractSeries *series, const quint8 channelNumber){

    if (series  && channelNumber < phase.size()) {

        QXYSeries *xySeries = static_cast<QXYSeries *>(series);
        // Use replace instead of clear + append, it's optimized for performance
//        for(int i = 0; i < sweep[sweepNumber][channelNumber].size(); i++){
//            qDebug() << channelNumber << sweep[sweepNumber][channelNumber].at(i);
//        }
//        qDebug() << "replace" << channelNumber << "ph" << phase[channelNumber];
        xySeries->replace(phase[channelNumber]);
    }
}

void PerchData::setPhase(QVector<float> phase,quint8 type){

    this->phase[type].clear();
    for(int i = 0; i < phase.size(); i++){
        if(phase.at(i) != -200){
            QPointF pointf;
            pointf.setX(i);
            pointf.setY(phase.at(i) / 2.0);
            this->phase[type].push_back(pointf);
        }
    }
}

void PerchData::setChannelCount(quint16 channel){
    if(mchannelCount != channel){

        mchannelCount = channel == 0 ? 1: channel;

        for(int i = 0; i < mchannelCount; i++)
            sweep[i].resize(mchannelCount);
    }
}

void PerchData::setLineType(LINETYPE linetype){
    qDebug() << "perchdata set line type" << (linetype == LINETYPE::LINE);
    if(this->linetype != linetype){
        this->linetype = linetype;        
    }
}

void PerchData::setTreshold(float treshold){
    QMetaObject::invokeMethod(okunSocket,"setTreshold",Q_ARG(float,treshold));
}

void PerchData::setDelta(float delta){
    QMetaObject::invokeMethod(okunSocket,"setDelta",Q_ARG(float,delta));
}

PerchData::~PerchData(){
    udpThread.quit();
    udpThread.wait();
}
