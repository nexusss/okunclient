import QtQuick 2.7
import QtCharts 2.1
import QtQuick.Layouts 1.1
import olvia.perchData 1.0
import "Control/AxisFunc.js" as AxisFunc
import "Control"

ChartView {
    id: chartView

    property real speedK: 100
    property string lineBorderColor: ""
    property int lineSeriesWidth: 4

    property real borderLeftValue: 0
    property real borderRightValue: 512
    property real borderBottomValue: 100

    property int xtickCount: 20
    property int ytickCount: 20
    property bool border: false
    property var sweepColorList: ""
    property var channelNameList: ""

    property bool yzeroIsMinimum: false //false меньше нуля показывать
    property bool xzeroIsMinimum: true //false меньше нуля показывать

    property var currentAxis: {"minAxisY1": 0,"maxAxisY1": 1000,"minAxisX1": 0,"maxAxisX1": 1000}

    property var perchData: 0

    readonly property var xAxis: axisX1
    readonly property var yAxis: axisY1
    property var refreshTimer: timer
//    animationOptions: ChartView.SeriesAnimations
    implicitWidth: 640
    implicitHeight: 320

    readonly property  int  minYValue : -100

    function initAxis(minY,maxY,minX,maxX){

        currentAxis = { "minAxisY1": AxisFunc.myfloor(minY),
                        "maxAxisY1": AxisFunc.myfloor(maxY),
                        "minAxisX1": AxisFunc.myfloor(minX),
                        "maxAxisX1": AxisFunc.myfloor(maxX)}
    }

    RowLayout{
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.top: parent.top
        anchors.topMargin: 5
        z: 10
        AxisEdit{
            id: yAxisEdit
            axis: "Y AXIS"
            onSelected: state ? z = 100 : z = 0
            max: axisY1.max
            min: axisY1.min
            onMinInput: {
                currentAxis.minAxisY1 = value
                axisY1.min = currentAxis.minAxisY1
            }
            onMaxInput: {
                currentAxis.maxAxisY1 = value
                axisY1.max = currentAxis.maxAxisY1
            }
            zeroIsMinimum: chartView.yzeroIsMinimum
            onMaxChanged: {
                ytickCount = AxisFunc.findTickCount(axisY1.max,axisY1.min)
            }
            onMinChanged: {
                ytickCount = AxisFunc.findTickCount(axisY1.max,axisY1.min)
            }
        }
        AxisEdit{
            id: xAxisEdit
            axis: "X AXIS"
            onSelected: state ? z = 100 : z = 0
            max: axisX1.max
            min: axisX1.min
            onMinInput: {
                currentAxis.minAxisX1 = value
                axisX1.min = currentAxis.minAxisX1
            }
            onMaxInput: {
                currentAxis.maxAxisX1 = value
                axisX1.max = currentAxis.maxAxisX1
            }
            zeroIsMinimum: chartView.xzeroIsMinimum
            onMaxChanged: {
                xtickCount = AxisFunc.findTickCount(axisX1.max,axisX1.min)
            }
            onMinChanged: {
                xtickCount = AxisFunc.findTickCount(axisX1.max,axisX1.min)
            }
        }
    }

    legend.visible: false
    ValueAxis {
        id: axisY1
        min: currentAxis.minAxisY1
        max: currentAxis.maxAxisY1
        tickCount: chartView.ytickCount
        gridLineColor: lineBorderColor
    }

    ValueAxis {
        id: axisX1
        min: currentAxis.minAxisX1
        max: currentAxis.maxAxisX1
        tickCount: chartView.xtickCount
        gridLineColor: lineBorderColor
    }

    Timer {
        id: timer
        interval: 1 / 60 * 1000 // 60 Hz
        running: chartView.visible
        repeat: true
    }

    MouseArea {
        id: mouseArea1
        anchors.fill: parent
        property int startMouseX: 0
        property int startMouseY: 0

        onPressed: {
            startMouseX = mouseX
            startMouseY = mouseY
        }
        onReleased: {
            startMouseX = 0
            startMouseY = 0
        }

        onMouseXChanged: {
            var pixels = mouseX - startMouseX
            startMouseX = mouseX
            chartView.scrollLeft(pixels)
            if(axisX1.min < 0 && xzeroIsMinimum === true){
                axisX1.min = 0
                axisX1.max = currentAxis.maxAxisX1
            }

            currentAxis.maxAxisX1 = axisX1.max
            currentAxis.minAxisX1 = axisX1.min
        }
        onMouseYChanged: {
            var pixels = mouseY - startMouseY
            startMouseY = mouseY
            chartView.scrollUp(pixels)
            if(axisY1.min < minYValue && yzeroIsMinimum === true){
                axisY1.min = minYValue
                axisY1.max = currentAxis.maxAxisY1
            }

            currentAxis.maxAxisY1 = axisY1.max
            currentAxis.minAxisY1 = axisY1.min
        }
    }

    function zoomY1(){
        var delta = axisY1.max - axisY1.min
        if (delta > 2){

            var deltaK = delta / speedK
            if (deltaK < 1)
                deltaK = 1

            currentAxis.minAxisY1 += deltaK
            currentAxis.maxAxisY1 -= deltaK

            axisY1.min = currentAxis.minAxisY1
            axisY1.max = currentAxis.maxAxisY1
        }
    }

    function unzoomY1(){
        var delta = axisY1.max - axisY1.min

        var deltaK = delta / speedK
        if (deltaK < 1)
            deltaK = 1
        currentAxis.minAxisY1 -= deltaK
        currentAxis.maxAxisY1 += deltaK

        axisY1.min = currentAxis.minAxisY1
        axisY1.max = currentAxis.maxAxisY1
    }

    function zoomX(){
        var delta = axisX1.max - axisX1.min
        if (delta > 2){

            var deltaK = delta / speedK
            if (deltaK < 1)
                deltaK = 1
            currentAxis.minAxisX1 += deltaK
            currentAxis.maxAxisX1 -= deltaK

            axisX1.min = currentAxis.minAxisX1
            axisX1.max = currentAxis.maxAxisX1
        }
    }

    function unzoomX(){
        var delta = axisX1.max - axisX1.min

        var deltaK = delta / speedK

        if (deltaK < 1)
            deltaK = 5
        currentAxis.minAxisX1 -= deltaK
        currentAxis.maxAxisX1 += deltaK

        axisX1.min = currentAxis.minAxisX1
        axisX1.max = currentAxis.maxAxisX1
    }
    function upY1(){
        var delta = axisY1.max - axisY1.min

        var deltaK = delta / speedK
        if (deltaK < 1)
            deltaK = 1

        if(currentAxis.minAxisY1 + deltaK < minYValue && yzeroIsMinimum === true){
            currentAxis.minAxisY1 = minYValue
        }
        else{
            currentAxis.minAxisY1 += deltaK
            currentAxis.maxAxisY1 += deltaK
        }

        axisY1.min = currentAxis.minAxisY1
        axisY1.max = currentAxis.maxAxisY1
    }

    function downY1(){
        var delta = axisY1.max - axisY1.min

        var deltaK = delta / speedK
        if (deltaK < 1)
            deltaK = 1

        if(currentAxis.minAxisY1 - deltaK < minYValue && yzeroIsMinimum === true){
            currentAxis.minAxisY1 = minYValue
        }
        else{
            currentAxis.minAxisY1 -= deltaK
            currentAxis.maxAxisY1 -= deltaK
        }

        axisY1.min = currentAxis.minAxisY1
        axisY1.max = currentAxis.maxAxisY1
    }

    function rightX(){
        var delta = axisX1.max - axisX1.min

        var deltaK = delta / speedK
        if (deltaK < 1)
            deltaK = 1

        if(currentAxis.minAxisX1 - deltaK < 0 && xzeroIsMinimum === true){
            currentAxis.minAxisX1 = 0
        }
        else{
            currentAxis.minAxisX1 -= deltaK
            currentAxis.maxAxisX1 -= deltaK
        }

        axisX1.min = currentAxis.minAxisX1
        axisX1.max = currentAxis.maxAxisX1

    }

    function leftX(){
        var delta = axisX1.max - axisX1.min

        var deltaK = delta / speedK
        if (deltaK < 1)
            deltaK = 1

        if(currentAxis.minAxisX1 - deltaK < 0 && xzeroIsMinimum === true){
            currentAxis.minAxisX1 = 0

        }
        else{
            currentAxis.minAxisX1 += deltaK
            currentAxis.maxAxisX1 += deltaK
        }

        axisX1.min = currentAxis.minAxisX1
        axisX1.max = currentAxis.maxAxisX1
    }
}
