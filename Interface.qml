import QtQuick 2.0
import QtQuick.Controls 2.0
import olvia.perchControl 1.0
import QtQuick.Layouts 1.1

Item {
    id: inter
    implicitWidth: 15
    implicitHeight: 300
    signal softwareSet(int softnumber)
    signal serverVersionChanged(string str)

    property var okunSettings: null
    function setPort(port){
        perchControl.udpport = port
    }

    function startTcpClient(){
        console.log("func start server");
        perchControl.startServer()
    }

    PerchControl{
        id: perchControl
        onServerVersionChanged: (serverVersion ===  "") ? inter.serverVersionChanged("Not connected to server") : inter.serverVersionChanged("connected to " + serverVersion)
        onSoftNumberIsSet: {
            softwareSet(softnumber)
            switch(softnumber){
            case 0:
                radioButton0.checked = true
                break;
            case 1:
                radioButton1.checked = true
                break;
            case 2:
                radioButton2.checked = true
                break
            }
        }

        onSendSettings: {

            okunSettings.leftI = leftI
            okunSettings.leftQ = leftQ

            okunSettings.upI = upI
            okunSettings.upQ = upQ

            okunSettings.rightI = rightI
            okunSettings.rightQ = rightQ

            okunSettings.downI = downI
            okunSettings.downQ = downQ



            okunSettings.treshold = treshold

            okunSettings.upCheckBox = up
            okunSettings.downCheckBox = down
            okunSettings.hammingCheckBox = hamming;
        }
    }



    Rectangle{
        x: 10
        y: 0
        width: 2
        height: parent.height
        color: "#2b2b2b"
        Rectangle {
            id: xLeftRec
            x: 0-width / 2
            width: 20
            height: 20
            color: "transparent"
            anchors.horizontalCenter: parent.left
            anchors.verticalCenter: parent.verticalCenter
            z: 10
            property bool hideState: true
            MouseArea {
                anchors.fill: parent
                onClicked: {
                    if(parent.hideState)
                        inter.implicitWidth = 170
                    else
                        inter.implicitWidth = 15

                    parent.hideState = !parent.hideState;

                }
            }

            Image {
                id: image1
                anchors.fill: parent
                source: parent.hideState ? "qrc:/icons/show.png" : "qrc:/icons/hide.png"
            }
        }
    }

    ColumnLayout{
        anchors.leftMargin: 15
        anchors.fill: parent
        Rectangle{
            color: "#ffffff"
            anchors.fill: parent
            ColumnLayout{


                Layout.fillHeight: true
                Layout.fillWidth: true
                Layout.minimumWidth: 150

                Text {
                    id: text1
                    text: qsTr("Software Type")
                    font.pixelSize: 12
                }

                RadioButton {
                    id: radioButton0
                    text: qsTr("0")                   
                    onClicked: {
                        perchControl.setSoftwareNumber(0)
                        softwareSet(0)
                    }
                }
                RadioButton {
                    id: radioButton1
                    text: qsTr("1")
                    onClicked: {
                        perchControl.setSoftwareNumber(1)
                        softwareSet(1)
                    }
                }
                RadioButton {
                    id: radioButton2
                    text: qsTr("2")
                    onClicked: {
                        perchControl.setSoftwareNumber(2)
//                        softwareSet(2)
                    }
                }

                Button{
                    text: qsTr("Settings")
                    onClicked: {
                        okunSettings.visible = !okunSettings.visible
                    }
                }
            }
        }
    }

    Component.onCompleted: {
        okunSettings.settingsChanges.connect(function(leftI,leftQ,
                                                      upI,upQ,
                                                      rightI,rightQ,
                                                      downI,downQ,
                                                      treshold,
                                                      up,down,hamming){

            perchControl.setSendSettings(leftI,leftQ,
                                         upI,upQ,
                                         rightI,rightQ,
                                         downI,downQ,
                                         treshold,
                                         up,down,hamming)
        })
    }
}
