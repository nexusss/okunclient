import QtQuick 2.0
import QtQuick.Controls 2.0
Rectangle {
    id: okunSettings
    width: 560
    height: 200

    property int leftI: 0
    property int leftQ: 0
    property int upI: 0
    property int upQ: 0
    property int rightI: 0
    property int rightQ: 0
    property int downI: 0
    property int downQ: 0

    property real treshold: -100
    property real delta: 0

    property bool upCheckBox: false
    property bool downCheckBox: false
    property bool hammingCheckBox: false

    signal settingsChanges(int leftI,int leftQ,
                           int upI,int upQ,
                           int rightI,int rightQ,
                           int downI,int downQ,
                           real treshold,
                           bool up,bool down,bool hamming)

    signal sendTresholdAndDelta(real treshold, real delta)

    Text {
        id: text1
        x: 8
        y: 43
        text: qsTr("Left channel")
        font.pixelSize: 20
    }

    Rectangle{
        x: 141
        y: 43
        width: 80
        height: 22
        border.width: 1
    TextInput {
        id: leftI_Input

        text: leftI
        horizontalAlignment: Text.AlignHCenter
        anchors.fill: parent

        font.pixelSize: 12
        validator: IntValidator{bottom: 0; top: 7;}

    }
    }

    Text {
        id: text2
        x: 170
        y: 17
        text: qsTr("I")
        font.pixelSize: 22
    }

    Rectangle{
        x: 227
        y: 43
        width: 80
        height: 22
        border.width: 1
    TextInput {
        id: leftQ_Input

        text: leftQ
        horizontalAlignment: Text.AlignHCenter
        anchors.fill: parent
        font.pixelSize: 12

        validator: IntValidator{bottom: 0; top: 7;}
    }
    }

    Text {
        id: text3
        x: 256
        y: 17
        text: qsTr("Q")
        font.pixelSize: 22
    }

    Text {
        id: text4
        x: 8
        y: 71
        text: qsTr("Up channel")
        font.pixelSize: 20
    }

    Rectangle{
        x: 141
        y: 71
        width: 80
        height: 22
        border.width: 1
    TextInput {
        id: upI_Input

        text: upI
        horizontalAlignment: Text.AlignHCenter
        anchors.fill: parent
        font.pixelSize: 12

        validator: IntValidator{bottom: 0; top: 7;}
    }
    }

    Rectangle{
        x: 227
        y: 71
        width: 80
        height: 22
        border.width: 1
    TextInput {
        id: upQ_Input

        text: upQ
        horizontalAlignment: Text.AlignHCenter
        anchors.fill: parent
        font.pixelSize: 12

        validator: IntValidator{bottom: 0; top: 7;}
    }
    }

    Text {
        id: text5
        x: 8
        y: 99
        text: qsTr("Right channel")
        font.pixelSize: 20
    }

    Rectangle{
        x: 141
        y: 99
        width: 80
        height: 22
        border.width: 1
    TextInput {
        id: rightI_Input

        text: rightI
        horizontalAlignment: Text.AlignHCenter
        anchors.fill: parent
        font.pixelSize: 12

        validator: IntValidator{bottom: 0; top: 7;}
    }
    }

    Rectangle{
        x: 227
        y: 99
        width: 80
        height: 22
        border.width: 1
    TextInput {
        id: rightQ_Input

        text: rightQ
        horizontalAlignment: Text.AlignHCenter
        anchors.fill: parent
        font.pixelSize: 12

        validator: IntValidator{bottom: 0; top: 7;}
    }
    }

    Text {
        id: text6
        x: 8
        y: 127
        text: qsTr("Down channel")
        font.pixelSize: 20
    }
Rectangle{
    x: 141
    y: 127
    width: 80
    height: 22
    border.width: 1
    TextInput {
        id: downI_Input

        text: downI
        horizontalAlignment: Text.AlignHCenter
        anchors.fill: parent
        font.pixelSize: 12

        validator: IntValidator{bottom: 0; top: 7;}
    }
}
Rectangle{
    x: 227
    y: 127
    width: 80
    height: 22
    border.width: 1
    TextInput {
        id: downQ_Input

        text: downQ
        horizontalAlignment: Text.AlignHCenter
        anchors.fill: parent
        font.pixelSize: 12
        validator: IntValidator{bottom: 0; top: 7;}
    }
}
    Text {
        id: text7
        x: 321
        y: 43
        text: qsTr("Treshold")
        font.pixelSize: 20
    }

    Rectangle{
        x: 409
        y: 43
        width: 80
        height: 22
        border.width: 1
    TextInput {
        id: tresholdInput

        text: treshold
        horizontalAlignment: Text.AlignHCenter
        anchors.fill: parent
        font.pixelSize: 12

    }
}
    CheckBox {
        id: upBox
        x: 321
        y: 109
        text: qsTr("Up")
        checked: upCheckBox
    }

    CheckBox {
        id: downBox
        x: 386
        y: 109
        text: qsTr("Down")
        checked: downCheckBox
    }

    Button {
        id: applybutton
        x: 160
        y: 160
        text: qsTr("Apply")
        anchors.horizontalCenter: parent.horizontalCenter
        onClicked: {
            settingsChanges(leftI_Input.text,leftQ_Input.text,
                            upI_Input.text,upQ_Input.text,
                            rightI_Input.text,rightQ_Input.text,
                            downI_Input.text,downQ_Input.text,
                            tresholdInput.text,
                            upBox.checked,downBox.checked,hammingBox.checked)
            okunSettings.visible = false

            sendTresholdAndDelta(tresholdInput.text,deltaInput.text)
        }
    }

    Text {
        id: text8
        x: 321
        y: 75
        width: 77
        height: 28
        text: qsTr("Delta")
        horizontalAlignment: Text.AlignHCenter
        verticalAlignment: Text.AlignVCenter
        font.pixelSize: 20
    }

    Rectangle {
        x: 409
        y: 75
        width: 80
        height: 22
        border.width: 1
        TextInput {
            id: deltaInput
            text: delta
            font.pixelSize: 12
            anchors.fill: parent
            horizontalAlignment: Text.AlignHCenter
        }
    }

    CheckBox {
        id: hammingBox
        x: 465
        y: 109
        text: qsTr("Hamming")
        checked: hammingCheckBox
    }
}
