#include "perchcontrol.h"
#include <QDataStream>

PerchControl::PerchControl(QObject *parent) : QObject(parent),
    tcpSocket(new QTcpSocket()),reconectTimer(new QTimer())
{

    tcpSocket->moveToThread(&tcpThread);
    reconectTimer->moveToThread(&tcpThread);
    reconectTimer->setInterval(500);
    connect(&tcpThread,&QThread::started,this,[=](){connectToServer(); reconectTimer->start();},Qt::DirectConnection);
    connect(reconectTimer,&QTimer::timeout,this,&PerchControl::connectToServer,Qt::DirectConnection);
    connect(&tcpThread,&QThread::destroyed,tcpSocket,&QTcpSocket::deleteLater);
    connect(&tcpThread,&QThread::destroyed,reconectTimer,&QTimer::deleteLater);
    connect(tcpSocket, &QTcpSocket::readyRead, this, &PerchControl::readAnswerFromServer);
    connect(tcpSocket, &QTcpSocket::connected, this, [=] () {qDebug() << "connected";QMetaObject::invokeMethod(reconectTimer,"stop");});
    connect(tcpSocket, &QTcpSocket::disconnected, this, [=] () {qDebug() << "disconnected"; QMetaObject::invokeMethod(reconectTimer,"start");});
    typedef void (QAbstractSocket::*QAbstractSocketErrorSignal)(QAbstractSocket::SocketError);
     qRegisterMetaType<QAbstractSocket::SocketError>("QAbstractSocket::SocketError");
    connect(tcpSocket, static_cast<QAbstractSocketErrorSignal>(&QAbstractSocket::error),
            this, [=] () {qDebug()<< "error" << tcpSocket->errorString();emit connectError(tcpSocket->errorString());});

    connect(this,&PerchControl::writeServer,tcpSocket,[=](QByteArray data){tcpSocket->write(data);});

}

QString PerchControl::ip(){
    return mip.toString();
}

quint16 PerchControl::port(){
    return mport;
}

QString PerchControl::serverVersion(){
    return mserverVersion;
}


void PerchControl::setIp(QString ip){
    if(mip.toString() != ip){
        mip.setAddress(ip);
    }
}

void PerchControl::setPort(quint16 port){
    if(mport != port){
        mport = port;
    }
}

quint16 PerchControl::udpport(){
    return mudpPort;
}

void PerchControl::setUDPPort(quint16 port){

    if(mudpPort != port){
        mudpPort = port;
    }
}

void PerchControl::setSoftwareNumber(int softnumber){
    QByteArray packet;
    QDataStream out(&packet,QIODevice::WriteOnly);
    out.setByteOrder(QDataStream::LittleEndian);
    out << quint16(packet.size());
    out << quint8(MESSAGETYPE::SOFTNUMBER);
    out << quint8(softnumber);
    out.device()->seek(0);

    out << quint16(packet.size() - sizeof(quint16));

    emit writeServer(packet);
}

void PerchControl::setSendSettings(quint8 leftI,quint8 leftQ, quint8 upI,quint8 upQ,quint8 rightI, quint8 rightQ,quint8 downI,quint8 downQ,float treshold, bool up, bool down, bool hamming){

    qDebug() << "set sendings" << leftI;
    QByteArray packet;
    QDataStream out(&packet,QIODevice::WriteOnly);
    out.setFloatingPointPrecision(QDataStream::SinglePrecision);
    out.setByteOrder(QDataStream::LittleEndian);
    out << quint16(packet.size());
    out << quint8(MESSAGETYPE::SETTINGSSET);
    out << quint8(leftI);
    out << quint8(leftQ);
    out << quint8(upI);
    out << quint8(upQ);
    out << quint8(rightI);
    out << quint8(rightQ);
    out << quint8(downI);
    out << quint8(downQ);
    out << treshold;
    out << quint8(up << 1 | down);
    out << quint8(hamming);

    out.device()->seek(0);

    qDebug() << "set sendings" << packet.size() << "hamming" << hamming;
    out << quint16(packet.size() - sizeof(quint16));

    emit writeServer(packet);

}

void PerchControl::startServer(){
    qDebug() << "start server";
    tcpThread.start();
}

void PerchControl::connectToServer(){


//    qDebug() << "conenct to server";

    if(rx.exactMatch(mip.toString())){
        tcpSocket->abort();
        tcpSocket->connectToHost(mip,mport);
    }
    else
        qDebug() << "not valid";

//    QMetaObject::invokeMethod(tcpSocket,"abort",);
//    QMetaObject::invokeMethod(tcpSocket,"connectToHost",Q_ARG(QHostAddress,ip),Q_ARG(quint16, port));
}

void PerchControl::readAnswerFromServer(){
    QByteArray read = tcpSocket->readAll();
    QDataStream in(&read,QIODevice::ReadOnly);
    in.setFloatingPointPrecision(QDataStream::SinglePrecision);
    in.setByteOrder(QDataStream::LittleEndian);

    quint16 blockSize;


    quint8 type;
    in >> blockSize >> type;
    switch(static_cast<ANSWERTYPE>(type)){
    case ANSWERTYPE::CONNECT:{
        qDebug() << "connect to server";
        quint8 majV;
        in >> majV;
        mserverVersion = "Server V. " + QString::number(majV) ;
        emit serverVersionChanged(mserverVersion);

        quint8 softN ,leftI,leftQ,upI,upQ,rightI,rightQ,downI,downQ;
        in >> softN;
        emit softNumberIsSet(softN);

        float treshold;
        quint8 sweep;
        quint8 hamming;

        in >> leftI >> leftQ >> upI >> upQ >> rightI >> rightQ >> downI >> downQ;

        in >> treshold >> sweep >> hamming;


    qDebug() << "hamming read" << hamming;

        emit sendSettings(leftI,leftQ,upI,upQ,rightI,rightQ,downI,downQ,treshold,sweep & 0x2,(sweep & 0x1), hamming >= 1);

        QByteArray packet;
        QDataStream out(&packet,QIODevice::WriteOnly);
        out.setByteOrder(QDataStream::LittleEndian);
        out << quint16(packet.size())
            << quint8(MESSAGETYPE::UDP)
            << mudpPort;


        out.device()->seek(0);
        out << quint16(packet.size() - sizeof(quint16));
        emit writeServer(packet);
    break;
    }
    case ANSWERTYPE::SOFTWARENUMBER:{

        quint8 softN;
        in >> softN;
        emit softNumberIsSet(softN);
    break;
    }
    }

}



PerchControl::~PerchControl(){
    tcpThread.quit();
    tcpThread.wait();
}
